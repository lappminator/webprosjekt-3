
<div class="row">
<div class="col-lg-6 col-lg-offset-3 itembox eventPageContainer">
<?php
$eventID = $_GET["EVENTID"];
$stmt = $db->prepare("SELECT * FROM EVENT 
	INNER JOIN USER ON EVENT.CREATOR=USER.USERID 
	WHERE EVENTID=? LIMIT 1");
	$stmt->execute(array("$eventID"));
	$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);


	while($row = $stmt->fetch(PDO::FETCH_ASSOC))	{
		$eventName = $row["EVENTNAME"];
		$eventImg = $row["EVENTIMG"];
		$eventDesc = $row["EVENTDESC"];
		$created = $row["CREATED"];
		$category = $row["CATEGORY"];
		$user = $row["USERNAME"];

//	EVENT PHOTO
		echo "<div class='row eventPageImg'>";
//		echo "<img src=" . $eventImg . ">";
		echo "<img src='". $eventImg ."' class='img-responsive'>";
		echo "</div>";

		echo "<div class='row'>";

// 	USER AND DATE CREATED
		echo "<div class='col-lg-3 eventPageDate'>";
		echo $category;
		echo "<br/>@ " . $user . " <br/>";
		echo $created;
		echo "</div>";


//	EVENT TITLE		
		echo "<div class='col-lg-6 eventPageTitle'><center>";
		echo $eventName;
		echo "</center></div>";


// 	DATE DET FAKTISK SKJER????
		echo "<div class='col-lg-12 eventPageWhen'><center>";
		echo "04.10.15 - 05.10.15";
		echo "</center></div>";


//	EVENT DESCRIPTION
		echo "<div class='col-lg-6 col-lg-offset-3 eventPageDesc'><center>";
		echo $eventDesc;
		echo "</center></div>";


};

?>

</div>

</div>