function checkName(){
  var name = document.getElementById("eventName").value;
  if (name == "" ){
    var message = "<p>Your event must have a name</p>";
    document.getElementById("eventNameMessage").innerHTML = message;
    return false;
  }
  else if (name != ""){
    var successMessage = "";
    document.getElementById("eventNameMessage").innerHTML = successMessage;
    return true;
  }
}
function checkDesc(){
  var desc = document.getElementById("description").value;
  if (desc == ""){
    var newMessage = "<p>Your event must have a description</p>";
    document.getElementById("eventDescMessage").innerHTML = newMessage;
    return false;
  }
  else if(desc !== ""){
    var successMessage = "";
    document.getElementById("eventDescMessage").innerHTML = successMessage;
    return true;
  }
}

function validateForm(){
  if(checkName() && checkDesc() ){
    return true;
  }
  else{
    return false;
  }
}
$( "form" ).on( "submit", function(event){
  
  if (!validateForm()) {
    event.preventDefault();
  }


  
} );