<div class="row header">
<div class="col-lg-2 logo">
  			<a href="index.php">Happit<span class="glyphicon glyphicon-sunglasses"></span></a>
  		</div>
  <div class="col-lg-3 search">
    <div class="input-group">
      <span class="input-group-btn"
        <button class="btn btn-default" type="button">Søk <span class="glyphicon glyphicon-search"></span></button>
      </span>
      <input type="text" class="form-control " placeholder="Search for events">
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->
  <div class="col-lg-1 navbutton ">
  	<a href="index.php"><button class="btn btn-default">HOME</button></a>
  </div>
  <div class="col-lg-1  navbutton">
  	<a href="utforsk.php"><button class="btn btn-default">EXPLORE</button></a>
  </div>

  <div class="col-lg-1 navbutton">
  	<a href="events.php"><button class="btn btn-default">MY EVENTS</button></a>
  </div>
  <div class="col-lg-3 col-lg-offset-1">
  <?php
  session_start();
    if (isset($_SESSION['logged']))  {
      echo "  	<div class='circle'>
  		<span class='glyphicon glyphicon-user user-icon'></span>
  		<a class='user' href='#' >" . $_SESSION['username'] . "</a>
  		  <ul class='dropdown'>
                <li><a href='#'>Settings</a></li>
                <li><a href='logout.php'>Logout</a></li>
            </ul>";
    } else {
      include "login.php";
    }
  ?>
  </div>
  <!--col lg 4-->
</div><!-- /.row -->
</div>