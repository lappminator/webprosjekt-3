<html>
<head>
<title>Title</title>
<meta charset="UTF-8"/>
<link rel="stylesheet" href="css/main.css" type="text/css" />
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</head>
<body>
 
<?php include('inc/header.php') ?>
<?php include('db/connect.php') ?>

<?php include('event_page.php') ?>


<?php
//File upload //
if(isset($_POST["submit"])){
$target_dir = "img/";
$target_file = $target_dir . basename($_FILES["postImg"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["postImg"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["postImg"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
/*
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
*/
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["postImg"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["postImg"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}



//prepare statementfor inserting new entry//
$instmt = $db->prepare("INSERT INTO POST (POSTTEXT,POSTIMG,EVENTID,USERID)".
					"VALUES(?,?,?,?);");
//get word from POST and protect from html entities
	$postText = $_POST['postText'];
	$user = "1";
	$event = "1";
	$postImg = $target_file;
	
	$instmt->execute(array("$postText","$postImg","$event", "$user"));
	echo "success";

}

?>




<form class="col-lg-4 col-lg-offset-4 addEventForm" action="post_form.php" method="POST" enctype="multipart/form-data">

<div class="input group input-group-md">
	<textarea type="text" name="postText" class="form-control eventDesc" placeholder="What's up?"></textarea>

</div>
<input type="file" name="postImg" id="postImg">

<button name="submit" class="col-lg-3 col-lg-offset-9 btn btn-default addEventButton">Add Picture</button>
</form>


<?php include('event_posts.php') ?>





</body>
</html>