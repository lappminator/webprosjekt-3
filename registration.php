<html>
<head>
    <title>HappIT</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="css/main.css" type="text/css" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</head>
<body>
<?php
    //REQUIRE DATABASE CONNECT FILE
    require_once 'db/connect.php';
    include 'inc/header.php';

    $stmt = $db->prepare("INSERT INTO USER (USERID, USERNAME, EMAIL, PASSWORD)"."VALUES(NULL,?,?,?);");

    if(isset($_POST['submit'])){
        $USERNAME = $_POST['username'];
        $PASSWORD = $_POST['password'];
        $HASH = password_hash($PASSWORD, PASSWORD_DEFAULT);
        $EMAIL = $_POST['email'];

        $stmt->execute(array("$USERNAME", "$EMAIL", "$HASH"));
        echo "Added a new user!";
    }
?>
<div class="row feed">
    <div class="col-lg-6 col-lg-offset-3">
        <div id="register">
            <h2>Registration</h2>
            <form action="registration.php" method="post">
                Username: <br><input class="form-control" type="text" name="username"><br>
                E-Mail: <br><input class="form-control" type="email" name="email"><br>
                Password: <br><input class="form-control" type="password" name="password"><br>
                <br>
                <input class="btn btn-default" type="submit" value="Register" name="submit"/>
            </form>
        </div>
</body>
</html>