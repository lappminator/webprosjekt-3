<html>
<head>
<title>Title</title>
<meta charset="utf-8"/>
<link rel="stylesheet" href="css/main.css" type="text/css" />
<!-- Latest compiled and minified CSS -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
</head>
<body>
 
<?php include('inc/header.php') ?>
<?php include('db/connect.php') ?>
<center><h2>Add a new event!</h2></center>

<?php

//hente kategoriene
$categoryQuery = $db->prepare("select * from CATEGORY");
$categoryQuery->execute();


//prepare statementfor inserting new entry//
$instmt = $db->prepare("INSERT INTO EVENT (EVENTNAME,EVENTDESC,EVENTIMG,LOCATION,CREATOR,CATEGORY)".
					"VALUES(?,?,?,?,?,?);");

//File upload //
if(isset($_POST["submit"])){
$target_dir = "img/";
$target_file = $target_dir . basename($_FILES["eventImg"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["eventImg"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["eventImg"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
/*
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
*/
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["eventImg"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["eventImg"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}


//get word from POST and protect from html entities

	$eventName = $_POST['eventName'];
	$description = $_POST['description'];
	$location = $_POST["eventLoc"];
	$eventImg = $target_file;
	$user = "1";
	$category = $_POST['selectCat'];
	
	$instmt->execute(array("$eventName","$description","$eventImg","$location", "$user","$category"));
	echo "<div class='successMessage'>You have successfully created the event: <b>$eventName</b></div>";
}
?>


<script type="text/javascript" src="js/eventValidation.js">
</script>


<form class="col-lg-4 col-lg-offset-4 addEventForm" onSubmit="return validateForm()" method="POST" enctype="multipart/form-data">
<div class="input group input-group-md">
	<input type="text" class="form-control" id="eventName" name="eventName" placeholder="Name your event"/>
</div>
<div id="eventNameMessage"></div>

<div class="input group input-group-md">
	<textarea type="text" id="description" name="description" class="form-control eventDesc" placeholder="Describe your event"></textarea>
</div>
<div id="eventDescMessage"></div>
<div class="input group input-group-md">
	<input type="text" class="form-control" id="eventLoc" name="eventLoc" placeholder="Where is your event"/>
</div>
<input type="file" name="eventImg" id="eventImg">
<select name="selectCat" class="col-lg-3 input group input-group-md selectCat">
		<?php 
		while($row = $categoryQuery->fetch(PDO::FETCH_ASSOC))	{
		$category = $row["CATEGORY"];
		echo "<option value='$category'>" . $category . "</option>";
	}
	?>
</select>
<button name="submit" class="col-lg-3 col-lg-offset-6 btn btn-default addEventButton">Add event</button>
</form>
<br>


 
</body>
</html>