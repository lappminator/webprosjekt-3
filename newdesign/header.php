<html>
<head>
    <title>HAPPIT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.indigo-pink.min.css">
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link rel="stylesheet" href="css/newdesign.css" type="text/css" />
    <script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
    <script>
        $(function(){
            $('a').each(function() {
                if ($(this).prop('href') == window.location.href) {
                    $(this).addClass('current');
                }
            });
        });
    </script>

</head>
<body>
<div class="mdl-layout mdl-js-layout">
    <header class="mdl-layout__header">
        <div class="mdl-layout__header-row">
            <span class="mdl-layout__title">HAPPIT</span>
            <div class="mdl-layout-spacer"></div>
            <nav class="mdl-navigation">
                <a class="mdl-navigation__link mdl-typography--text-uppercase" href="new_index.php">Home</a>
                <a class="mdl-navigation__link mdl-typography--text-uppercase" href="#">Explore</a>
                <a class="mdl-navigation__link mdl-typography--text-uppercase" href="new_events.php">My Events</a>
            </nav>
            <div class="mdl-layout-spacer"></div>
            <form action="#">
                <div class="mdl-textfield mdl-js-textfield" id="login_panel">
                    <input class="mdl-textfield__input" type="text" placeholder="Username" id="username">
                    <input class="mdl-textfield__input" type="password" placeholder="Password" id="spassword">
                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Login</button>
                </div>
            </form>
            <a href="registration.php"><button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="reg">Sign up</button></a>
            <form action="#">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                    <label class="mdl-button mdl-js-button mdl-button--icon" for="search">
                        <i class="material-icons">search</i>
                    </label>
                    <div class="mdl-textfield__expandable-holder">
                        <input class="mdl-textfield__input" type="text" id="search">
                    </div>
                </div>
            </form>
        </div>
    </header>