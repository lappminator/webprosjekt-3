 <?php include ('newdesign/header.php') ?>
            <main class="mdl-layout__content">
                <div class="demo-card-wide mdl-card mdl-shadow--2dp">
                    <div class="mdl-card__title">
                        <h2 class="mdl-card__title-text">Exemple Event</h2>
                        <div class="mdl-layout-spacer"></div>
                        <h6 id="made_by">Shabby, 13:37, 13.11.2015</h6>
                    </div>
                    <img src="http://stagephod.org/wp-content/uploads/2015/02/Events-stagephod.jpg"/>
                    <div class="mdl-card__supporting-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer aliquet ultricies accumsan. Etiam sodales interdum massa, ac gravida neque auctor et. Integer leo lorem, suscipit sit amet massa ut, eleifend aliquet mauris. Donec pulvinar eros ipsum, sit amet vehicula mauris fringilla id.
                    </div>
                    <div class="mdl-card__actions mdl-card--border">
                        <div class="mdl-card__supporting-text" id="social">
                            <i class="material-icons">star_border</i>
                            <h6 class="mdl-typography--text-uppercase">6 Like it</h6>
                            <h6 class="mdl-typography--text-uppercase" id="comment">Comments: 4</h6>
                        </div>
                    </div>
                </div>
                <div class="demo-card-wide mdl-card mdl-shadow--2dp">
                    <div class="mdl-card__title">
                        <h2 class="mdl-card__title-text">Exemple Event</h2>
                        <div class="mdl-layout-spacer"></div>
                        <h6 id="made_by">Shabby, 13:37, 13.11.2015</h6>
                    </div>
                    <img src="http://www.atpi.com/assets/Uploads/_resampled/SetWidth1920-A-championship-event10.jpg"/>
                    <div class="mdl-card__supporting-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer aliquet ultricies accumsan. Etiam sodales interdum massa, ac gravida neque auctor et. Integer leo lorem, suscipit sit amet massa ut, eleifend aliquet mauris. Donec pulvinar eros ipsum, sit amet vehicula mauris fringilla id.
                    </div>
                    <div class="mdl-card__actions mdl-card--border">
                        <div class="mdl-card__supporting-text" id="social">
                            <i class="material-icons">star_border</i>
                            <h6 class="mdl-typography--text-uppercase">6 Like it</h6>
                            <h6 class="mdl-typography--text-uppercase" id="comment">Comments: 4</h6>
                        </div>
                    </div>
                </div>
                <?php include('newdesign/footer.php') ?>