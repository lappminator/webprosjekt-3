<?php

if(!isset($_SESSION)){
    session_start();
}

require_once 'db/connect.php';

if(isset($_POST['submit'])){
    $error_msg = '';
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);

    if($username == '')
        $error_msg = 'You must enter your username!';

    if($password == '')
        $error_msg = 'You must enter your password';

    if($error_msg == '') {
        $stmt = $db->prepare('SELECT * FROM USER WHERE USERNAME = :username');
        $stmt->bindParam(':username', $username);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if(count($row) > 0 && password_verify($password, $row['PASSWORD'])){
            $_SESSION['logged'] = true;
            $_SESSION['username'] = $row['USERNAME'];
            $_SESSION['userID'] = $row['USERID'];
            header('Location:index.php');
            exit;
        }else{
            $error_msg = "Username and password are not found";
        }
    }
    }
?>
<div id="login">
            <?php
            if(isset($error_msg)){
                echo $error_msg;
            }
            ?>
            <form class="form-inline" action="login.php" method="post">
                <div class="form-group">
                    <input class="form-control input-sm" id="usernameInput" type="text" name="username" placeholder="Username">
                </div>
                <div class="form-group">
                    <input class="form-control input-sm" id="passwordInput" type="password" name="password" placeholder="Password">
                </div>
                    <input class="btn btn-default btn-sm" type="submit" value="Login" name="submit"/> <a href="registration.php">Reg</a>
            </form>
</div>