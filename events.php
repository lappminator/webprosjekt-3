<html>
    <head>
        <title>Eventer</title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/main.css" type="text/css" />
        <link rel="stylesheet" href="css/gallery.css" type="text/css" />
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.2/masonry.pkgd.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    </head>

    <body>
        <div id="container wrapper">
            <?php include ('inc/header.php') ?>

            <a href="event_form.php">
            <div class="col-lg-12 col-lg-offset-4">
                <button type="button" class="btn btn-primary col-lg-4">Lag et event!</button>
            </div>
        </a>
            <div class="row events">
                        <?php include ('inc/eventfeed.php') ?>
            </div>
        </div>
    </body>
</html>